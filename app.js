/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/
var score=[0,0],roundScore=0,activePlayer,img,temp;
var newGame = document.querySelector('.btn-new');
var rollDice = document.querySelector('.btn-roll');
var holdDice = document.querySelector('.btn-hold');
activePlayer = 0;
rollDice.addEventListener('click',roll);
img = document.querySelectorAll('img');
img[0].style.display = 'none';
img[1].style.display = 'none';
function roll(){
	var rand1 = Math.floor(Math.random()*6+1);
	var rand2 = Math.floor(Math.random()*6+1);
	//display the dice images
	img[0].src="dice-"+rand1+'.png';
	img[1].src="dice-"+rand2+'.png';	
	if(rand1!==1 && rand2!==1){		
		img[0].style.display = 'block';
		img[1].style.display = 'block';
		roundScore=parseInt(document.querySelector('#current-'+activePlayer).textContent);
 		roundScore+=rand1+rand2;
 		document.querySelector('#current-'+activePlayer).textContent=roundScore;
	}
	else{
		img[0].style.display = 'none';
		img[1].style.display = 'none';
 		document.querySelector('#current-'+activePlayer).textContent='0';
 		//here the controller must go the second one
 		//class must get toggled
 		nextMember();
	}
}
function nextMember(){
	activePlayer ===0 ? activePlayer=1 : activePlayer=0;
	document.querySelector('.player-0-panel').classList.toggle('active');
	document.querySelector('.player-1-panel').classList.toggle('active');
}
holdDice.addEventListener('click',hold);
function hold(){
	temp = parseInt(document.querySelector('#score-'+activePlayer).textContent);
	temp+= parseInt(document.querySelector('#current-'+activePlayer).textContent);
	document.querySelector('#score-'+activePlayer).textContent = temp;
	//here check the condition if the score is greater than 100 winner should be announced..
	temp=parseInt(document.querySelector('.final-score').value);
	temp=temp||100;
	document.querySelector('.final-score').setAttribute('disabled','');
	if(document.querySelector('#score-'+activePlayer).textContent>=temp){
 		document.querySelector('#name-'+activePlayer).textContent = 'Winner!';
 		document.querySelector('.player-'+activePlayer+'-panel').className+=' winner';
 		document.querySelector('.player-'+activePlayer+'-panel').classList.toggle('active');
		rollDice.removeEventListener('click',roll);
		holdDice.removeEventListener('click',hold);
		img[0].style.display = 'none';
		img[1].style.display = 'none';
	}
	else{
		img[0].style.display = 'none';
		img[1].style.display = 'none';	
		document.querySelector('#current-'+activePlayer).textContent = 0;
		nextMember();
	}	
}
newGame.addEventListener('click',init);
function init(){
	//reset the game
	document.querySelector('#score-0').textContent ='0';
	document.querySelector('#score-1').textContent ='0';
	document.querySelector('#name-0').textContent ='player 1';
	document.querySelector('#name-1').textContent ='player 2';
	document.querySelector('#current-0').textContent = '0';
	document.querySelector('#current-1').textContent = '0';	
	rollDice.addEventListener('click',roll);
	holdDice.addEventListener('click',hold);
	document.querySelector('.player-'+activePlayer+'-panel').classList.toggle('active');
	document.querySelector('.player-'+activePlayer+'-panel').className='player-'+activePlayer+'-panel';
	document.querySelector('.final-score').removeAttribute('disabled','');
	activePlayer=0;
	document.querySelector('.player-'+activePlayer+'-panel').className='player-'+activePlayer+'-panel';
	document.querySelector('.player-'+activePlayer+'-panel').classList.toggle('active');	
}